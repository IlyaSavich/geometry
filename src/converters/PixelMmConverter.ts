export const MM_IN_PIXEL = 3.77952755905511;

export function convertFromPixelToMm(value: number) {
    return value * MM_IN_PIXEL;
}
