import * as React from 'react';
import { connect } from 'react-redux';
import { IStore } from 'store/store';
import { Dispatch } from 'redux';
import { DrawingConfigActions } from 'store/drawingConfig/reducer';
import * as actions from 'store/drawingConfig/actions';
import { FigureType } from 'configs/figures';
import unfinishedFigureProcessor from 'plot/figures/processors/UnfinishedFigureProcessor';
import figureTypeMenu from 'configs/figureTypeMenu';

type ReduxType = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

class App extends React.Component<ReduxType> {
    public render() {
        return (
            <div className="App">
                <div className="container">
                    <div id="canvas-wrapper">
                        <canvas id="main" width="600" height="600"/>
                    </div>
                </div>
                {this.renderFigureTypeMenu()}
            </div>
        );
    }

    private renderFigureTypeMenu() {
        return figureTypeMenu.map((figureTypeMenuItem) => (
            <button
                key={figureTypeMenuItem.type}
                onClick={() => this.props.selectFigureType(figureTypeMenuItem.type)}
            >
                {figureTypeMenuItem.label}
            </button>
        ));
    }
}

const mapStateToProps = (state: IStore) => {
    return { figureType: state.drawingConfig.figureType };
};

const mapDispatchToProps = (dispatch: Dispatch<DrawingConfigActions>) => {
    return {
        selectFigureType: (figureType: FigureType) => {
            dispatch(actions.selectFigureType(figureType));
            unfinishedFigureProcessor.setType(figureType);
        },
    };
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;
