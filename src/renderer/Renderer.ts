import BackgroundGridLayer from 'renderer/layers/BackgroundGridLayer';
import BaseLayer from 'renderer/layers/BaseLayer';
import eventBus, { Events } from 'events/EventBus';
import UnfinishedFigureLayer from 'renderer/layers/UnfinishedFigureLayer';
import FiguresLayer from 'renderer/layers/FiguresLayer';
import MeasurementsLayer from 'renderer/layers/MeasurementsLayer';

const LAYERS = [
    BackgroundGridLayer,
    FiguresLayer,
    MeasurementsLayer,
    UnfinishedFigureLayer,
];

class Renderer {
    private layers: BaseLayer[] = [];

    public init() {
        this.layers = LAYERS.map((Layer) => new Layer());

        eventBus.subscribe(Events.onAppInitialized, () => this.renderAll());
    }

    private renderAll() {
        this.layers.map((layer) => layer.draw());
    }
}

export default new Renderer();
