import drawingConfig from 'configs/drawingConfig';
import { IItem } from 'plot/services/ItemStorage';
import hoverItemService from 'services/detection/HoverItemService';
import selectionItemService from 'services/detection/SelectionItemService';

export function getColour(item: IItem): string {
    if (hoverItemService.isDetected(item)) {
        return drawingConfig.figures.colours.hovered;
    }

    if (selectionItemService.isDetected(item)) {
        return drawingConfig.figures.colours.selected;
    }

    return drawingConfig.figures.colours.base;
}
