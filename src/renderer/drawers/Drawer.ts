abstract class Drawer {
    protected readonly context: CanvasRenderingContext2D;

    constructor(context: CanvasRenderingContext2D) {
        this.context = context;
    }

    public abstract draw(...values: any[]): void;
}

export default Drawer;
