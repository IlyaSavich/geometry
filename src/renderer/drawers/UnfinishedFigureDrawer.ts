import Vertex from 'plot/vertices/Vertex';
import drawingConfig from 'configs/drawingConfig';
import UnfinishedFigure from 'plot/figures/unfinished/UnfinishedFigure';
import Drawer from 'renderer/drawers/Drawer';
import VertexDrawer from 'renderer/drawers/VertexDrawer';
import mouse from 'services/Mouse';
import Edge from 'plot/edges/Edge';
import Vector2d from 'plot/engine/Vector2d';

class UnfinishedFigureDrawer extends Drawer {
    private vertexDrawer: VertexDrawer;

    constructor(context: CanvasRenderingContext2D) {
        super(context);

        this.vertexDrawer = new VertexDrawer(context);
    }

    public draw(figure: UnfinishedFigure) {
        this.drawVertices(figure);
        this.drawPolygon(figure);
        this.drawTemp(figure);
    }

    protected drawPolygon(figure: UnfinishedFigure) {
        const vertices = figure.getVertices();
        const context = this.context;

        context.save();

        context.lineWidth = drawingConfig.figures.edges.lineWidth;
        context.strokeStyle = drawingConfig.figures.colours.base;
        context.beginPath();

        vertices.reduce((previousVertex: Vertex | null, vertex) => {
            context.moveTo(vertex.getX(), vertex.getY());
            if (previousVertex) {
                context.lineTo(previousVertex.getX(), previousVertex.getY());
            }
            return vertex;
        }, null);

        context.stroke();

        context.restore();
    }

    private drawVertices(figure: UnfinishedFigure) {
        this.vertexDrawer.draw(...figure.getVertices());
    }

    private drawTemp(figure: UnfinishedFigure) {
        const mousePosition = mouse.getPosition();
        const tempVertex = new Vertex(new Vector2d(mousePosition.getX(), mousePosition.getY()));

        const context = this.context;

        context.save();

        context.lineWidth = drawingConfig.figures.vertices.lineWidth;
        context.strokeStyle = drawingConfig.figures.colours.shadowed;
        context.beginPath();
        context.arc(tempVertex.getX(), tempVertex.getY(), drawingConfig.figures.vertices.radius, 0, 2 * Math.PI);
        context.stroke();
        context.restore();

        if (figure.getVertices().length === 0) {
            return;
        }

        const vertices = figure.getVertices();
        const prevVertex = vertices[vertices.length - 1];
        const tempEdge = new Edge([prevVertex, tempVertex]);

        context.save();
        context.beginPath();

        context.lineWidth = drawingConfig.figures.edges.lineWidth;
        context.strokeStyle = drawingConfig.figures.colours.shadowed;

        context.moveTo(tempEdge.getStart().getX(), tempEdge.getStart().getY());
        context.lineTo(tempEdge.getEnd().getX(), tempEdge.getEnd().getY());

        context.stroke();
        context.restore();
    }
}

export default UnfinishedFigureDrawer;
