import Figure from 'plot/figures/base/Figure';
import Drawer from 'renderer/drawers/Drawer';
import drawingConfig from 'configs/drawingConfig';
import VertexDrawer from 'renderer/drawers/VertexDrawer';
import * as drawingHelper from 'renderer/helpers/DrawingHelper';

class FigureDrawer extends Drawer {
    private vertexDrawer: VertexDrawer;

    constructor(context: CanvasRenderingContext2D) {
        super(context);

        this.vertexDrawer = new VertexDrawer(context);
    }

    public draw(figure: Figure) {
        this.drawVertices(figure);
        this.drawPolygon(figure);
    }

    protected drawPolygon(figure: Figure) {
        const edges = figure.getEdges();
        const context = this.context;

        context.save();

        context.lineWidth = drawingConfig.figures.edges.lineWidth;

        edges.forEach((edge) => {
            context.beginPath();

            context.strokeStyle = drawingHelper.getColour(edge);

            context.moveTo(edge.getStart().getX(), edge.getStart().getY());
            context.lineTo(edge.getEnd().getX(), edge.getEnd().getY());

            context.stroke();
        });

        context.restore();
    }

    private drawVertices(figure: Figure) {
        this.vertexDrawer.draw(...figure.getVertices());
    }
}

export default FigureDrawer;
