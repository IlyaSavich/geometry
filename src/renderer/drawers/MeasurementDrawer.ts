import Drawer from 'renderer/drawers/Drawer';
import BaseMeasurement from 'plot/measurements/entities/BaseMeasurement';

class MeasurementDrawer extends Drawer {
    public draw(measurement: BaseMeasurement): void {
        const value = measurement.getValue();
        const position = measurement.getPosition();

        const context = this.context;

        context.save();

        context.font = '50px Arial';
        context.fillText(value.toFixed(2), position.getX(), position.getY());

        context.restore();
    }
}

export default MeasurementDrawer;
