import Drawer from 'renderer/drawers/Drawer';
import Vertex from 'plot/vertices/Vertex';
import drawingConfig from 'configs/drawingConfig';
import * as drawingHelper from 'renderer/helpers/DrawingHelper';

class VertexDrawer extends Drawer {
    public draw(...vertices: Vertex[]) {
        vertices.forEach((vertex) => this.drawVertex(vertex));
    }

    private drawVertex(vertex: Vertex) {
        const context = this.context;

        context.save();

        context.lineWidth = drawingConfig.figures.vertices.lineWidth;
        context.strokeStyle = drawingHelper.getColour(vertex);
        context.beginPath();
        context.arc(vertex.getX(), vertex.getY(), drawingConfig.figures.vertices.radius, 0, 2 * Math.PI);
        context.stroke();

        context.restore();
    }
}

export default VertexDrawer;
