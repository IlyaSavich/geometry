import BaseLayer from 'renderer/layers/BaseLayer';
import eventBus, { Events } from 'events/EventBus';
import unfinishedFigureProcessor from 'plot/figures/processors/UnfinishedFigureProcessor';
import UnfinishedFigureDrawer from 'renderer/drawers/UnfinishedFigureDrawer';

class UnfinishedFigureLayer extends BaseLayer {
    private drawer: UnfinishedFigureDrawer;

    constructor() {
        super();
        this.drawer = new UnfinishedFigureDrawer(this.canvas.getContext());

        eventBus.subscribe(Events.onAddUnfinishedVertex, () => this.redraw());
        eventBus.subscribe(Events.onAddFigure, () => this.clear());
        eventBus.subscribe(Events.onMouseMove, () => this.redraw());
    }

    public draw(): void {
        const figure = unfinishedFigureProcessor.getFigure();

        if (!figure) {
            return;
        }

        this.drawer.draw(figure);
    }
}

export default UnfinishedFigureLayer;
