import BaseLayer from 'renderer/layers/BaseLayer';
import itemStorage from 'plot/services/ItemStorage';
import FigureDrawer from 'renderer/drawers/FigureDrawer';
import eventBus, { Events } from 'events/EventBus';

class FiguresLayer extends BaseLayer {
    private drawer: FigureDrawer;

    constructor() {
        super();
        this.drawer = new FigureDrawer(this.canvas.getContext());

        eventBus.subscribe(Events.onAddFigure, () => this.redraw());
        eventBus.subscribe(Events.onHoverChanged, () => this.redraw());
        eventBus.subscribe(Events.onSelectionChanged, () => this.redraw());
    }

    public draw(): void {
        const figures = itemStorage.getFigures();

        figures.forEach((figure) => this.drawer.draw(figure));
    }
}

export default FiguresLayer;
