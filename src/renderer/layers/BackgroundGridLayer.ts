import BaseLayer from 'renderer/layers/BaseLayer';
import drawingConfig from 'configs/drawingConfig';

const CELL_DIMENSION = drawingConfig.backgroundGrid.cellDimension;

class BackgroundGridLayer extends BaseLayer {
    public draw(): void {
        const context = this.canvas.getContext();
        const canvasDimensions = this.canvas.getDimensions();

        context.save();

        context.strokeStyle = drawingConfig.backgroundGrid.colour;
        context.lineWidth = drawingConfig.backgroundGrid.lineWidth;
        context.beginPath();

        for (let i = CELL_DIMENSION; i < canvasDimensions.width; i += CELL_DIMENSION) {
            context.moveTo(i, 0);
            context.lineTo(i, canvasDimensions.height);
        }

        for (let j = 0; j < canvasDimensions.height; j += CELL_DIMENSION) {
            context.moveTo(0, j);
            context.lineTo(canvasDimensions.width, j);
        }

        context.stroke();

        context.restore();
    }
}

export default BackgroundGridLayer;
