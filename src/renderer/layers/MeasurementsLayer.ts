import BaseLayer from 'renderer/layers/BaseLayer';
import eventBus, { Events } from 'events/EventBus';
import MeasurementDrawer from 'renderer/drawers/MeasurementDrawer';
import measurementService from 'plot/measurements/services/MeasurementService';
import selectionItemService from 'services/detection/SelectionItemService';

class MeasurementsLayer extends BaseLayer {
    private drawer: MeasurementDrawer;

    constructor() {
        super();
        this.drawer = new MeasurementDrawer(this.canvas.getContext());

        eventBus.subscribe(Events.onAddMeasurement, () => this.redraw());
        eventBus.subscribe(Events.onSelectionChanged, () => this.redraw());
    }

    public draw(): void {
        const selectedItem = selectionItemService.getDetectedItem();

        if (!selectedItem) {
            return;
        }

        const measurements = measurementService.getForItemIds(selectedItem.getRelatedItemIds());
        measurements.forEach((measurement) => this.drawer.draw(measurement));
    }
}

export default MeasurementsLayer;
