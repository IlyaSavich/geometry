import CanvasDecorator from 'services/CanvasDecorator';

abstract class BaseLayer {
    protected canvas: CanvasDecorator;

    constructor() {
        const canvasElement = document.createElement('canvas');
        this.canvas = new CanvasDecorator(canvasElement);

        const canvasWrapper = document.getElementById('canvas-wrapper')!;
        canvasWrapper.appendChild(canvasElement);
    }

    public abstract draw(): void;

    protected redraw() {
        this.clear();
        this.draw();
    }

    protected clear() {
        const { width, height } = this.canvas.getDimensions();

        this.canvas.getContext().clearRect(0, 0, width, height);
    }
}

export default BaseLayer;
