import * as converters from 'converters';
import eventBus, { Events } from 'events/EventBus';
import mouse from 'services/Mouse';
import Vector2d from 'plot/engine/Vector2d';

class MainCanvasEventInitializer {
    public init() {
        const canvas = document.getElementById('main') as HTMLCanvasElement | null;

        if (!canvas) {
            throw new Error('canvas#main was not found');
        }

        canvas.addEventListener('mousedown', () => {
            mouse.setMouseDown(true);

            eventBus.publish(Events.onMouseDown);
        });

        canvas.addEventListener('mouseup', () => {
            if (mouse.isDragging()) {
                mouse.setDragging(false);
                eventBus.publish(Events.onMouseDragEnd);
            }

            mouse.setMouseDown(false);

            eventBus.publish(Events.onMouseUp);

            if (!mouse.isDragging()) {
                eventBus.publish(Events.onMouseClick);
            }
        });

        canvas.addEventListener('mousemove', (e) => {
            const position = this.getConvertedEventCoordinates(e, canvas);
            mouse.setPosition(position);

            if (mouse.isMouseDown() && !mouse.isDragging())  {
                mouse.setDragging(true);
                eventBus.publish(Events.onMouseDragStart);
            }

            eventBus.publish(Events.onMouseMove);
        });
    }

    private getConvertedEventCoordinates(e: MouseEvent, canvas: HTMLCanvasElement): Vector2d {
        const boundingRect = canvas.getBoundingClientRect();
        const x = converters.convertFromPixelToMm(e.x - boundingRect.left);
        const y = converters.convertFromPixelToMm(e.y - boundingRect.top);

        return new Vector2d(x, y);
    }
}

export default new MainCanvasEventInitializer();
