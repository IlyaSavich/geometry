import eventBus, { Events } from 'events/EventBus';
import renderer from 'renderer/Renderer';
import itemStorage from 'plot/services/ItemStorage';
import unfinishedFigureProcessor from 'plot/figures/processors/UnfinishedFigureProcessor';
import mainCanvasEventInitializer from 'app/MainCanvasEventInitializer';
import measurementService from 'plot/measurements/services/MeasurementService';
import hoverItemService from 'services/detection/HoverItemService';
import selectionItemService from 'services/detection/SelectionItemService';

export function init() {
    mainCanvasEventInitializer.init();
    hoverItemService.init();
    selectionItemService.init();
    renderer.init();
    itemStorage.init();
    unfinishedFigureProcessor.init();
    measurementService.init();

    eventBus.publish(Events.onAppInitialized);
}
