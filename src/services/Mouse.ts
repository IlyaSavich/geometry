import Vector2d from 'plot/engine/Vector2d';

class Mouse {
    private position: Vector2d;
    private mouseDown: boolean;
    private dragging: boolean;

    public getPosition(): Vector2d {
        return this.position;
    }

    public setPosition(position: Vector2d) {
        this.position = position;
    }

    public isMouseDown(): boolean {
        return this.mouseDown;
    }

    public setMouseDown(mouseDown: boolean) {
        this.mouseDown = mouseDown;
    }

    public isDragging(): boolean {
        return this.dragging;
    }

    public setDragging(dragging: boolean) {
        this.dragging = dragging;
    }
}

export default new Mouse();
