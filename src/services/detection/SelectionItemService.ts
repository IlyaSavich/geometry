import eventBus, { Events } from 'events/EventBus';
import DetectionService from 'services/detection/DetectionService';
import { IItem } from 'plot/services/ItemStorage';

class SelectionItemService extends DetectionService {
    public init() {
        eventBus.subscribe(Events.onMouseClick, () => {
            const shouldChangeHover = this.detectItem();

            if (shouldChangeHover) {
                eventBus.publish(Events.onSelectionChanged, this.detectedItem);
            }
        });
    }

    public setSelection(item: IItem): void {
        this.detectedItem = item;
        eventBus.publish(Events.onSelectionChanged, this.detectedItem);
    }
}

export default new SelectionItemService();
