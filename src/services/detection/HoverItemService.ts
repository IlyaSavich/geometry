import eventBus, { Events } from 'events/EventBus';
import DetectionService from 'services/detection/DetectionService';

class HoverItemService extends DetectionService {
    public init() {
        eventBus.subscribe(Events.onMouseMove, () => {
            const shouldChangeHover = this.detectItem();

            if (shouldChangeHover) {
                eventBus.publish(Events.onHoverChanged);
            }
        });
    }
}

export default new HoverItemService();
