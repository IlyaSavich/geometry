import itemStorage, { IItem } from 'plot/services/ItemStorage';
import { IItemDetector } from 'services/detection/detectors/ItemDetector';
import edgeDetector from 'services/detection/detectors/EdgeDetector';
import vertexDetector from 'services/detection/detectors/VertexDetector';
import * as itemHelper from 'plot/helpers/ItemHelper';

const DETECTORS: IItemDetector[] = [
    vertexDetector,
    edgeDetector,
];

export default abstract class DetectionService {
    protected detectedItem?: IItem;

    public isDetected(item: IItem): boolean {
        return !!this.detectedItem && this.detectedItem.getRelatedItemIds().includes(item.itemId);
    }

    public getDetectedItem(): IItem | undefined {
        return this.detectedItem;
    }

    protected detectItem() {
        const items = itemStorage.getItems().sort(this.sortItems);

        const detectedItem = items.find((item) =>
            DETECTORS.some((detector) =>
                detector.isApplicable(item) && detector.isDetected(item),
            ),
        );

        const shouldChangeDetection = detectedItem && !this.detectedItem ||
            !detectedItem && this.detectedItem ||
            detectedItem && this.detectedItem && detectedItem.itemId !== this.detectedItem.itemId;

        if (shouldChangeDetection) {
            this.detectedItem = detectedItem;
        }

        return shouldChangeDetection;
    }

    private sortItems = (item1: IItem, item2: IItem) => {
        const priority1 = this.getItemPriority(item1);
        const priority2 = this.getItemPriority(item2);

        return priority2 - priority1;
    };

    private getItemPriority(item: IItem): number {
        if (itemHelper.isFigure(item)) {
            return 0;
        }

        if (itemHelper.isEdge(item)) {
            return 1;
        }

        if (itemHelper.isVertex(item)) {
            return 2;
        }

        if (itemHelper.isMeasurement(item)) {
            return 3;
        }

        return -1;
    }
}
