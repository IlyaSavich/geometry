import Vertex from 'plot/vertices/Vertex';
import * as geometryHelper from 'plot/helpers/GeometryHelper';
import mouse from 'services/Mouse';
import drawingConfig from 'configs/drawingConfig';
import { IItem } from 'plot/services/ItemStorage';
import * as itemHelper from 'plot/helpers/ItemHelper';
import { IItemDetector } from 'services/detection/detectors/ItemDetector';

class VertexDetector implements IItemDetector {
    public isApplicable(item: IItem) {
        return itemHelper.isVertex(item);
    }

    public isDetected(vertex: Vertex) {
        const distanceToVertexCenter = geometryHelper.getDistance(vertex.getVector(), mouse.getPosition());

        return distanceToVertexCenter < drawingConfig.figures.vertices.radius + drawingConfig.figures.vertices.lineWidth;
    }
}

export default new VertexDetector();
