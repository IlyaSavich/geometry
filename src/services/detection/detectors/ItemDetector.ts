import { IItem } from 'plot/services/ItemStorage';

export interface IItemDetector {
    isApplicable(item: IItem): boolean;
    isDetected(item: IItem): boolean;
}
