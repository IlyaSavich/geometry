import { IItem } from 'plot/services/ItemStorage';
import * as itemHelper from 'plot/helpers/ItemHelper';
import * as geometryHelper from 'plot/helpers/GeometryHelper';
import Vector2d from 'plot/engine/Vector2d';
import * as edgeHelper from 'plot/edges/EdgeHelper';
import Edge from 'plot/edges/Edge';
import mouse from 'services/Mouse';
import { IItemDetector } from 'services/detection/detectors/ItemDetector';

type DetectionArea = [Vector2d, Vector2d, Vector2d, Vector2d];

interface IDetectionAreaCache {
    [itemId: number]: DetectionArea;
}

const DETECTION_DISTANCE = 30;

class EdgeDetector implements IItemDetector {
    private detectionAreaCache: IDetectionAreaCache = {};

    public isApplicable(item: IItem) {
        return itemHelper.isEdge(item);
    }

    public isDetected(edge: Edge) {
        if (!this.detectionAreaCache[edge.itemId]) {
            this.detectionAreaCache[edge.itemId] = this.calculateDetectionArea(edge);
        }

        return geometryHelper.isVectorInsideRectangle(
            mouse.getPosition(), this.detectionAreaCache[edge.itemId],
        );
    }

    private calculateDetectionArea(edge: Edge): DetectionArea {
        const adjustment = edgeHelper.getNormal(edge).scalar(DETECTION_DISTANCE);

        const start = edge.getStart().getVector();
        const end = edge.getEnd().getVector();

        return [
            start.add(adjustment),
            start.subtract(adjustment),
            end.subtract(adjustment),
            end.add(adjustment),
        ];
    }
}

export default new EdgeDetector();
