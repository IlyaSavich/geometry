import * as converters from 'converters';
import { MM_IN_PIXEL } from 'converters/PixelMmConverter';

// in application we operate with mm instead of pixels.
const DEFAULT_SCALE = 1 / MM_IN_PIXEL;
const CANVAS_WIDTH = 600;
const CANVAS_HEIGHT = 600;

class CanvasDecorator {
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
        this.canvas.width = CANVAS_WIDTH;
        this.canvas.height = CANVAS_HEIGHT;

        this.initContext();
    }

    public getDimensions() {
        return {
            width: converters.convertFromPixelToMm(this.canvas.width),
            height: converters.convertFromPixelToMm(this.canvas.height),
        };
    }

    public getContext() {
        return this.context;
    }

    private initContext() {
        this.context = this.canvas.getContext('2d')!;

        this.setDefaultContextScale();
    }

    private setDefaultContextScale() {
        this.context.scale(DEFAULT_SCALE, DEFAULT_SCALE);
    }
}

export default CanvasDecorator;
