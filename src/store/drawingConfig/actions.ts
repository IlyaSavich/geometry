import { FigureType } from 'configs/figures';
import { action } from 'typesafe-actions';
import { SELECT_FIGURE_TYPE } from 'store/drawingConfig/actionTypes';

export function selectFigureType(figureType: FigureType) {
    return action(SELECT_FIGURE_TYPE, { figureType });
}
