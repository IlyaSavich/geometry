import { FigureType } from 'configs/figures';
import { SELECT_FIGURE_TYPE } from 'store/drawingConfig/actionTypes';
import { ActionType } from 'typesafe-actions';
import * as actions from 'store/drawingConfig/actions';

export type DrawingConfigActions = ActionType<typeof actions>;

export interface IDrawingConfigState {
    figureType?: FigureType;
}

const defaultState: IDrawingConfigState = {
    figureType: undefined,
};

export default function drawingConfig(state: IDrawingConfigState = defaultState, action: DrawingConfigActions): IDrawingConfigState {
    switch (action.type) {
        case SELECT_FIGURE_TYPE:
            return { ...state, figureType: action.payload.figureType };
        default:
            return state;
    }
}
