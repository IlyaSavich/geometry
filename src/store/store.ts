import { combineReducers, createStore } from 'redux';
import drawingConfigReducer, { IDrawingConfigState } from 'store/drawingConfig/reducer';

export interface IStore {
    drawingConfig: IDrawingConfigState;
}

const store = createStore<IStore, any, any, any>(
    combineReducers({
        drawingConfig: drawingConfigReducer,
    }),
);

export default store;
