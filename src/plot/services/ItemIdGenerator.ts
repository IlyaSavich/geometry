class ItemIdGenerator {
    private itemId: number = 0;

    public getNewItemId() {
        return ++this.itemId;
    }
}

export default new ItemIdGenerator();
