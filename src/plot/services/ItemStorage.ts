import * as _ from 'lodash';
import Figure from 'plot/figures/base/Figure';
import eventBus, { Events } from 'events/EventBus';
import itemIdGenerator from 'plot/services/ItemIdGenerator';
import BaseMeasurement from 'plot/measurements/entities/BaseMeasurement';
import * as itemHelper from 'plot/helpers/ItemHelper';

export interface IItem {
    itemId: number;
    getRelatedItemIds(): number[];
}

interface IItemsMap {
    [itemId: number]: IItem;
}

class ItemStorage {
    private items: IItemsMap = {};

    public init() {
        eventBus.subscribe(Events.onAddMeasurement, (measurement: BaseMeasurement) => this.addItems(measurement));
    }

    public getFigures(): Figure[] {
        return this.getItems().filter((item: IItem) => itemHelper.isFigure(item)) as Figure[];
    }

    public getItems(): IItem[] {
        return Object.values(this.items);
    }

    public addItems(...items: IItem[]) {
        const flattenItems = _.flatten<IItem>(items);

        flattenItems.forEach((item) => {
            item.itemId = itemIdGenerator.getNewItemId();
            this.items[item.itemId] = item;
        });
    }
}

export default new ItemStorage();
