const DEFAULT_EPS = 0.000001;

export function equals(a: number, b: number, eps: number = DEFAULT_EPS) {
    return Math.abs(a - b) < eps;
}
