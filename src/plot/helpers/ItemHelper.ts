import { IItem } from 'plot/services/ItemStorage';
import Figure from 'plot/figures/base/Figure';
import Edge from 'plot/edges/Edge';
import Vertex from 'plot/vertices/Vertex';
import BaseMeasurement from 'plot/measurements/entities/BaseMeasurement';

export function isFigure(item: IItem): item is Figure {
    return item instanceof Figure;
}

export function isEdge(item: IItem): item is Edge {
    return item instanceof Edge;
}

export function isVertex(item: IItem): item is Vertex {
    return item instanceof Vertex;
}

export function isMeasurement(item: IItem): item is BaseMeasurement {
    return item instanceof BaseMeasurement;
}
