import Vector2d from 'plot/engine/Vector2d';
import * as arrayHelper from 'helpers/ArrayHelper';

enum Position {
    left = 'left',
    right = 'right',
    equal = 'equal',
}

export function getDistance(firstVector: Vector2d, secondVector: Vector2d) {
    const diffX = secondVector.getX() - firstVector.getX();
    const diffY = secondVector.getY() - firstVector.getY();

    return Math.sqrt(diffX * diffX + diffY * diffY);
}

export function isVectorInsideRectangle(vector: Vector2d, rectangle: [Vector2d, Vector2d, Vector2d, Vector2d]): boolean {
    const lines = arrayHelper.linkedCycledChunks(rectangle, 2) as [[Vector2d, Vector2d]];
    const positions = lines.map((line) => getPositionRelativeToLine(vector, line));

    return positions.every((position) => position === positions[0]);
}

function getPositionRelativeToLine(vector: Vector2d, line: [Vector2d, Vector2d]): string {
    const criterion = (vector.getX() - line[0].getX()) * (line[1].getY() - line[0].getY()) -
        (vector.getY() - line[0].getY()) * (line[1].getX() - line[0].getX());

    if (criterion < 0) {
        return Position.left;
    }

    if (criterion > 0) {
        return Position.right;
    }

    return Position.equal;
}
