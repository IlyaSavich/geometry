import Vector2d from 'plot/engine/Vector2d';
import { IItem } from 'plot/services/ItemStorage';

class Vertex implements IItem {
    public itemId: number;
    private vector: Vector2d;

    constructor(vector: Vector2d) {
        this.vector = vector;
    }

    public getRelatedItemIds() {
        return [this.itemId];
    }

    public getVector(): Vector2d {
        return this.vector;
    }

    public setVector(vector: Vector2d) {
        this.vector = vector;
    }

    public getX(): number {
        return this.vector.getX();
    }

    public getY(): number {
        return this.vector.getY();
    }
}

export default Vertex;
