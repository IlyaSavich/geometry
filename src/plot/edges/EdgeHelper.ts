import Edge from 'plot/edges/Edge';
import * as geometryHelper from 'plot/helpers/GeometryHelper';
import Vector2d from 'plot/engine/Vector2d';

export function getLength(edge: Edge): number {
    const start = edge.getStart().getVector();
    const end = edge.getEnd().getVector();

    return geometryHelper.getDistance(start, end);
}

export function getNormal(edge: Edge): Vector2d {
    const start = edge.getStart().getVector();
    const end = edge.getEnd().getVector();

    return start.subtract(end).perpendicular().normalized();
}

export function getCenter(edge: Edge): Vector2d {
    const start = edge.getStart().getVector();
    const end = edge.getEnd().getVector();

    const amendX = (end.getX() - start.getX()) / 2;
    const amendY = (end.getY() - start.getY()) / 2;

    return start.add(new Vector2d(amendX, amendY));
}
