import Vertex from 'plot/vertices/Vertex';
import { IItem } from 'plot/services/ItemStorage';
import * as _ from 'lodash';

type EdgeVertices = [Vertex, Vertex];

class Edge implements IItem {
    public itemId: number;
    private vertices: EdgeVertices;

    constructor(vertices: EdgeVertices) {
        this.setVertices(vertices);
    }

    public getRelatedItemIds() {
        return [
            this.itemId,
            ..._.flatMap(this.vertices, (vertex) => vertex.getRelatedItemIds()),
        ];
    }

    public getVertices(): EdgeVertices {
        return this.vertices;
    }

    public setVertices(vertices: EdgeVertices) {
        this.vertices = vertices;
    }

    public getStart(): Vertex {
        return this.vertices[0];
    }

    public getEnd(): Vertex {
        return this.vertices[1];
    }
}

export default Edge;
