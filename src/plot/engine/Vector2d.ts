import * as fuzzyMath from 'plot/helpers/fuzzyMath';

class Vector2d {
    private readonly x: number;
    private readonly y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    public getX(): number {
        return this.x;
    }

    public getY(): number {
        return this.y;
    }

    public equals(vector: Vector2d): boolean {
        return fuzzyMath.equals(this.x, vector.x) && fuzzyMath.equals(this.y, vector.y);
    }

    public scalar(value: number): Vector2d {
        return new Vector2d(this.x * value, this.y * value);
    }

    public perpendicular(): Vector2d {
        return new Vector2d(-this.y, this.x);
    }

    public add(vector: Vector2d): Vector2d {
        return new Vector2d(this.x + vector.x, this.y + vector.y);
    }

    public subtract(vector: Vector2d): Vector2d {
        return new Vector2d(this.x - vector.x, this.y - vector.y);
    }

    public normalized(): Vector2d {
        const length = Math.sqrt(this.x * this.x + this.y * this.y);

        return new Vector2d(this.x / length, this.y / length);
    }
}

export default Vector2d;
