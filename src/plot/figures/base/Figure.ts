import * as _ from 'lodash';
import { FigureType } from 'configs/figures';
import Edge from 'plot/edges/Edge';
import Vertex from 'plot/vertices/Vertex';
import { IItem } from 'plot/services/ItemStorage';

abstract class Figure implements IItem {
    public itemId: number;
    protected edges: Edge[] = [];

    public getEdges(): Edge[] {
        return this.edges;
    }

    public getRelatedItemIds() {
        return [
            this.itemId,
            ..._.flatMap(this.edges, (edge: Edge) => edge.getRelatedItemIds()),
        ];
    }

    public getVertices(): Vertex[] {
        return _(this.edges)
            .map<Edge[], Vertex[]>((edge: Edge) => edge.getVertices())
            .flatten()
            .uniqBy((vertex: Vertex) => vertex.itemId)
            .value();
    }

    public abstract getType(): FigureType;
}

export default Figure;
