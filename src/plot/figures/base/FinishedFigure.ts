import Figure from 'plot/figures/base/Figure';
import figures from 'configs/figures';
import Edge from 'plot/edges/Edge';

abstract class FinishedFigure extends Figure {
    constructor(edges: Edge[]) {
        super();

        const verticesCount = edges.length;
        const allowedVerticesCount = figures[this.getType()].verticesCount;

        if (allowedVerticesCount !== verticesCount) {
            throw new Error(this.getType() + ': expected to be ' + allowedVerticesCount + 'edgess. Got ' + verticesCount);
        }

        this.edges = edges;
    }
}

export default FinishedFigure;
