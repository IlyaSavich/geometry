import eventBus, { Events } from 'events/EventBus';
import UnfinishedFigure from 'plot/figures/unfinished/UnfinishedFigure';
import itemStorage from 'plot/services/ItemStorage';
import Vector2d from 'plot/engine/Vector2d';
import figures, { FigureType, IFigureConfig } from 'configs/figures';
import Vertex from 'plot/vertices/Vertex';
import Edge from 'plot/edges/Edge';
import Figure from 'plot/figures/base/Figure';
import * as arrayHelper from 'helpers/ArrayHelper';
import selectionItemService from 'services/detection/SelectionItemService';
import mouse from 'services/Mouse';

class UnfinishedFigureProcessor {
    private figure: UnfinishedFigure | null = null;
    private figureConfig: IFigureConfig;

    public init() {
        eventBus.subscribe(Events.onMouseClick, () => this.onMouseClick());
    }

    public getFigure() {
        return this.figure;
    }

    public setType(type: FigureType) {
        this.figure = new UnfinishedFigure(type);
        this.figureConfig = figures[type];
    }

    private onMouseClick() {
        if (!this.figure) {
            return;
        }

        const mousePosition = mouse.getPosition();

        this.addVertex(mousePosition.getX(), mousePosition.getY());

        if (this.isFigureFinished()) {
            this.addFigure();
            return;
        }
    }

    private addVertex(x: number, y: number) {
        const vertex = new Vertex(new Vector2d(x, y));
        this.figure!.addVertex(vertex);

        eventBus.publish(Events.onAddUnfinishedVertex, vertex);
    }

    private isFigureFinished() {
        const vertices = this.figure!.getVertices();
        const requiredVerticesCount = this.figureConfig.verticesCount;

        return vertices.length === requiredVerticesCount;
    }

    private addFigure() {
        const figure = this.figure!;
        const vertices = figure.getVertices();
        const edges = this.getEdgesFromVertices(vertices);

        const Constructor = figures[figure.getType()].entity;
        const finishedFigure = new Constructor(edges);
        this.figure = null;

        itemStorage.addItems(finishedFigure, ...edges, ...vertices);
        this.fireEventsOnFinishFigure(finishedFigure);
        selectionItemService.setSelection(finishedFigure);
    }

    private getEdgesFromVertices(vertices: Vertex[]): Edge[] {
        return arrayHelper.linkedCycledChunks(vertices, 2)
            .map((edgeVertices: [Vertex, Vertex]) => new Edge(edgeVertices));
    }

    private fireEventsOnFinishFigure(figure: Figure) {
        eventBus.publish(Events.onAddFigure, figure);

        figure.getEdges().forEach((edge) => eventBus.publish(Events.onAddEdge, edge));
        figure.getVertices().forEach((vertex) => eventBus.publish(Events.onAddVertex, vertex));
    }
}

export default new UnfinishedFigureProcessor();
