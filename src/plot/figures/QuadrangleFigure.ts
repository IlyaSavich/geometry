import { FigureType } from 'configs/figures';
import FinishedFigure from 'plot/figures/base/FinishedFigure';

class QuadrangleFigure extends FinishedFigure {
    public getType(): FigureType {
        return FigureType.quadrangle;
    }
}

export default QuadrangleFigure;
