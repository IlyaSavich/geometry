import { FigureType } from 'configs/figures';
import Vertex from 'plot/vertices/Vertex';

class UnfinishedFigure {
    private readonly type: FigureType;
    private vertices: Vertex[] = [];

    constructor(type: FigureType) {
        this.type = type;
    }

    public addVertex(vertex: Vertex) {
        this.vertices.push(vertex);
    }

    public getVertices(): Vertex[] {
        return this.vertices;
    }

    public getType() {
        return this.type;
    }
}

export default UnfinishedFigure;
