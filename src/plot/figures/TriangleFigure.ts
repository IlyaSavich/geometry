import { FigureType } from 'configs/figures';
import FinishedFigure from 'plot/figures/base/FinishedFigure';

class TriangleFigure extends FinishedFigure {
    public getType(): FigureType {
        return FigureType.triangle;
    }
}

export default TriangleFigure;
