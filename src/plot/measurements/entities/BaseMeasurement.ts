import Vector2d from 'plot/engine/Vector2d';
import { IItem } from 'plot/services/ItemStorage';

abstract class BaseMeasurement implements IItem {
    public itemId: number;
    protected value: number;
    protected position: Vector2d;

    public getValue(): number {
        return this.value;
    }

    public getPosition(): Vector2d {
        return this.position;
    }

    public calculate(): void {
        this.value = this.calculateValue();
        this.position = this.calculatePosition();
    }

    public abstract getRelatedItemIds(): number[];
    protected abstract calculateValue(): number;
    protected abstract calculatePosition(): Vector2d;
}

export default BaseMeasurement;
