import BaseMeasurement from 'plot/measurements/entities/BaseMeasurement';
import Edge from 'plot/edges/Edge';
import * as edgeHelper from 'plot/edges/EdgeHelper';

class LengthMeasurement extends BaseMeasurement {
    private readonly edge: Edge;

    constructor(edge: Edge) {
        super();
        this.edge = edge;
    }

    public getRelatedItemIds(): number[] {
        return [this.edge.itemId];
    }

    protected calculateValue() {
        return edgeHelper.getLength(this.edge);
    }

    protected calculatePosition() {
        const edgeCenter = edgeHelper.getCenter(this.edge);
        const edgeNormal = edgeHelper.getNormal(this.edge);

        return edgeCenter.add(edgeNormal.scalar(25));
    }
}

export default LengthMeasurement;
