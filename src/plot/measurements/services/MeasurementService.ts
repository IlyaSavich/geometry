import * as _ from 'lodash';
import eventBus, { Events } from 'events/EventBus';
import BaseMeasurement from 'plot/measurements/entities/BaseMeasurement';
import LengthMeasurement from 'plot/measurements/entities/LengthMeasurement';
import { Constructor } from 'configs/general';

interface IMeasurementsMapping {
    [itemId: number]: BaseMeasurement[];
}

type CreateMeasurementEventMapping = {
    [eventName in Events]?: Array<Constructor<BaseMeasurement>>;
};

const CREATE_MEASUREMENT_EVENTS: CreateMeasurementEventMapping = {
    [Events.onAddEdge]: [LengthMeasurement],
};

class MeasurementService {
    private measurements: IMeasurementsMapping = {};

    public init() {
        _.forEach(CREATE_MEASUREMENT_EVENTS, (Measurements: Array<Constructor<BaseMeasurement>>, eventName: Events) => {
            eventBus.subscribe(eventName, (...args: any[]) => {
                Measurements.forEach((Measurement) => {
                    const newMeasurement = new Measurement(...args);
                    newMeasurement.calculate();
                    this.addMeasurement(newMeasurement.getRelatedItemIds(), newMeasurement);

                    eventBus.publish(Events.onAddMeasurement, newMeasurement);
                });
            });
        });
    }

    public getAllUnique(): BaseMeasurement[] {
        return _(this.measurements).values().flatten().uniqBy((measurement) => measurement.itemId).value();
    }

    public getForItemIds(itemIds: number[]): BaseMeasurement[] {
        return _(itemIds).map((itemId) => this.measurements[itemId] || []).flatten().value();
    }

    private addMeasurement(itemIds: number[], measurement: BaseMeasurement) {
        itemIds.forEach((itemId) => {
            if (!this.measurements[itemId]) {
                this.measurements[itemId] = [];
            }
            this.measurements[itemId].push(measurement);
        });
    }
}

export default new MeasurementService();
