export default {
    backgroundGrid: {
        cellDimension: 100,
        colour: '#9876AA',
        lineWidth: 1,
    },
    figures: {
        colours: {
            base: '#000000',
            shadowed: '#7e7e7f',
            hovered: '#287BDE',
            selected: '#c0ae09',
        },
        vertices: {
            lineWidth: 10,
            radius: 20,
        },
        edges: {
            lineWidth: 2,
        },
    },
};
