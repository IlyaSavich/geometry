import Figure from 'plot/figures/base/Figure';
import TriangleFigure from 'plot/figures/TriangleFigure';
import QuadrangleFigure from 'plot/figures/QuadrangleFigure';
import Edge from 'plot/edges/Edge';

export enum FigureType {
    triangle = 'triangle',
    quadrangle = 'quadrangle',
}

interface IFinishedFigureConstructor {
    new (vertices: Edge[]): Figure;
}

export interface IFigureConfig {
    entity: IFinishedFigureConstructor;
    verticesCount: number;
}

type Configs = {
    [T in FigureType]: IFigureConfig;
};

const configs: Configs = {
    [FigureType.triangle]: {
        entity: TriangleFigure,
        verticesCount: 3,
    },
    [FigureType.quadrangle]: {
        entity: QuadrangleFigure,
        verticesCount: 4,
    },
};

export default configs;
