import { FigureType } from 'configs/figures';

const figureTypeMenu = [
    {
        type: FigureType.triangle,
        label: 'Triangle',
    },
    {
        type: FigureType.quadrangle,
        label: 'Quadrangle',
    },
];

export default figureTypeMenu;
