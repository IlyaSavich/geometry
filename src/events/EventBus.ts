export enum Events {
    // general app events
    onAppInitialized = 'onAppInitialized',

    // canvas events
    onMouseClick = 'onMouseClick',
    onMouseMove = 'onMouseMove',
    onMouseDown = 'onMouseDown',
    onMouseUp = 'onMouseUp',
    onMouseDragStart = 'onMouseDragStart',
    onMouseDragEnd = 'onMouseDragEnd',

    // figures events
    onAddFigure = 'onAddFigure',
    onAddEdge = 'onAddEdge',
    onAddVertex = 'onAddVertex',
    onAddUnfinishedVertex = 'onAddUnfinishedVertex',

    // measurements events
    onAddMeasurement = 'onAddMeasurement',

    // items events
    onHoverChanged = 'onHoverChanged',
    onSelectionChanged = 'onSelectionChanged',
}

type Listener = (...args: any[]) => any;

type Listeners = {
    [type in Events]?: Listener[];
};

class EventBus {
    private listeners: Listeners = {};

    public subscribe(type: Events, handle: Listener) {
        if (this.listeners.hasOwnProperty(type)) {
            this.listeners[type]!.push(handle);
        } else {
            this.listeners[type] = [handle];
        }
    }

    public unsubscribe(type: Events, handle: Listener) {
        if (!this.listeners.hasOwnProperty(type)) {
            return;
        }

        this.listeners[type] = this.listeners[type]!.filter((listener) => listener !== handle);
    }

    public publish(type: Events, ...args: any[]) {
        const listeners = this.listeners[type];

        if (!listeners) {
            return;
        }

        listeners.forEach((handle) => handle(...args));
    }
}

export default new EventBus();
