export function linkedCycledChunks<T>(array: T[], chunkItemsCount: number): T[][] {
    let chunkIndex = 0;
    const chunks: T[][] = [];

    array.forEach((item) => {
        if (!chunks[chunkIndex]) {
            chunks[chunkIndex] = [];
        }

        chunks[chunkIndex].push(item);

        if (chunks[chunkIndex].length === chunkItemsCount) {
            chunkIndex++;
            chunks[chunkIndex] = [item];
        }
    });

    chunks[chunks.length - 1].push(array[0]);

    return chunks;
}
